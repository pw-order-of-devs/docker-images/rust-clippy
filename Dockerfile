FROM rust:1.65-slim-buster
MAINTAINER Paweł Walus <losseheil.aikanar@gmail.com>

RUN apt update -y && apt install pkg-config libssl-dev openssh-client -y

RUN rustup component add clippy
RUN cargo install cargo-tarpaulin